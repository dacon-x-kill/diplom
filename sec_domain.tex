\section{Обзор литературы}
\label{sec:domain}

\subsection{Кластеризация}
\label{sub:domain:clustering_algorithm}

Кластеризация (или кластерный анализ) — это задача разбиения множества объектов на группы, называемые кластерами. Внутри каждой группы должны оказаться «похожие» объекты, а объекты разных группы должны быть как можно более отличны. Главное отличие кластеризации от классификации состоит в том, что перечень групп четко не задан и определяется в процессе работы алгоритма~\cite{mandel}.

Применение кластерного анализа в общем виде сводится к следующим этапам:

\begin{itemize}
\item Отбор выборки объектов для кластеризации.
\item Определение множества переменных, по которым будут оцениваться объекты в выборке. При необходимости – нормализация значений переменных.
\item Вычисление значений меры сходства между объектами.
\item Применение метода кластерного анализа для создания групп сходных объектов (кластеров).
\item Представление результатов анализа.
\end{itemize}

После получения и анализа результатов возможна корректировка выбранной метрики и метода кластеризации до получения оптимального результата.

Кластеризация в Data Mining приобретает ценность тогда, когда она выступает одним из этапов анализа данных, построения законченного аналитического решения. Аналитику часто легче выделить группы схожих объектов, изучить их особенности и построить для каждой группы отдельную модель, чем создавать одну общую модель для всех данных. 

\subsubsection {Меры растояний}

Итак, как же определять схожесть объектов? Для начала нужно составить вектор характеристик для каждого объекта — как правило, это набор числовых значений, например, рост-вес человека. Однако существуют также алгоритмы, работающие с качественными (т.н. категорийными) характеристиками.

После того, как мы определили вектор характеристик, можно провести нормализацию, чтобы все компоненты давали одинаковый вклад при расчете «расстояния». В процессе нормализации все значения приводятся к некоторому диапазону, например, [-1, -1] или [0, 1]~\cite{aivaz}.

\subsubsection {Классификация алгоритмов}

\textit{Иерархические и плоские}

Иерархические алгоритмы (также называемые алгоритмами таксономии) строят не одно разбиение выборки на непересекающиеся кластеры, а систему вложенных разбиений. То есть на выходе мы получаем дерево кластеров, корнем которого является вся выборка, а листьями — наиболее мелкие кластера.

Плоские алгоритмы строят одно разбиение объектов на кластеры.

\textit{Четкие и нечеткие}

Четкие (или непересекающиеся) алгоритмы каждому объекту выборки ставят в соответствие номер кластера, т.е. каждый объект принадлежит только одному кластеру. Нечеткие (или пересекающиеся) алгоритмы каждому объекту ставят в соответствие набор вещественных значений, показывающих степень отношения объекта к кластерам. Т.е. каждый объект относится к каждому кластеру с некоторой вероятностью~\cite{clust_lect}.

\subsubsection{Объединение кластеров}

В случае использования иерархических алгоритмов встает вопрос, как объединять между собой кластера, как вычислять «расстояния» между ними. Существует несколько метрик:
\begin{enumerate}[label=\arabic*.]
\item Одиночная связь (расстояния ближайшего соседа). В этом методе расстояние между двумя кластерами определяется расстоянием между двумя наиболее близкими объектами (ближайшими соседями) в различных кластерах. Результирующие кластеры имеют тенденцию объединяться в цепочки.

\item Полная связь (расстояние наиболее удаленных соседей). В этом методе расстояния между кластерами определяются наибольшим расстоянием между любыми двумя объектами в различных кластерах (т.е. наиболее удаленными соседями). Этот метод обычно работает очень хорошо, когда объекты происходят из отдельных групп. Если же кластеры имеют удлиненную форму или их естественный тип является «цепочечным», то этот метод непригоден.

\item Невзвешенное попарное среднее. В этом методе расстояние между двумя различными кластерами вычисляется как среднее расстояние между всеми парами объектов в них. Метод эффективен, когда объекты формируют различные группы, однако он работает одинаково хорошо и в случаях протяженных («цепочечного» типа) кластеров.

\item Взвешенное попарное среднее. Метод идентичен методу невзвешенного попарного среднего, за исключением того, что при вычислениях размер соответствующих кластеров (т.е. число объектов, содержащихся в них) используется в качестве весового коэффициента. Поэтому данный метод должен быть использован, когда предполагаются неравные размеры кластеров.

\item Невзвешенный центроидный метод. В этом методе расстояние между двумя кластерами определяется как расстояние между их центрами тяжести.

\item Взвешенный центроидный метод (медиана). Этот метод идентичен предыдущему, за исключением того, что при вычислениях используются веса для учета разницы между размерами кластеров. Поэтому, если имеются или подозреваются значительные отличия в размерах кластеров, этот метод оказывается предпочтительнее предыдущего.
\end{enumerate}

\subsubsection{Обзор алгоритмов}

Визуальное сравнение работы алгоритмов кластеризации на различный данных приведено на рисунке 1.1.

\begin{figure}[ht]
\centering
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{clust_algs.png}  
  \caption{Алгоритмы кластеризации}
  \label{fig:domain:algs}
\end{figure}

\begin{enumerate}[label=\arabic*.]

\item Алгоритмы иерархической кластеризации

Среди алгоритмов иерархической кластеризации выделяются два основных типа: восходящие и нисходящие алгоритмы. Нисходящие алгоритмы работают по принципу «сверху-вниз»: в начале все объекты помещаются в один кластер, который затем разбивается на все более мелкие кластеры. Более распространены восходящие алгоритмы, которые в начале работы помещают каждый объект в отдельный кластер, а затем объединяют кластеры во все более крупные, пока все объекты выборки не будут содержаться в одном кластере. Таким образом строится система вложенных разбиений. Результаты таких алгоритмов обычно представляют в виде дерева – дендрограммы. Классический пример такого дерева – классификация животных и растений~\cite{kotov}.

Для вычисления расстояний между кластерами чаще все пользуются двумя расстояниями: одиночной связью или полной связью (см. обзор мер расстояний между кластерами).

К недостатку иерархических алгоритмов можно отнести систему полных разбиений, которая может являться излишней в контексте решаемой задачи~\cite{clust_lect}.

\item Алгоритмы квадратичной ошибки

Задачу кластеризации можно рассматривать как построение оптимального разбиения объектов на группы. При этом оптимальность может быть определена как требование минимизации среднеквадратической ошибки разбиения
Алгоритмы квадратичной ошибки относятся к типу плоских алгоритмов. Самым распространенным алгоритмом этой категории является метод k-средних. Этот алгоритм строит заданное число кластеров, расположенных как можно дальше друг от друга. Работа алгоритма делится на несколько этапов:

    Случайно выбрать k точек, являющихся начальными «центрами масс» кластеров.
    Отнести каждый объект к кластеру с ближайшим «центром масс».
    Пересчитать «центры масс» кластеров согласно их текущему составу.
    Если критерий остановки алгоритма не удовлетворен, вернуться к п. 2.


В качестве критерия остановки работы алгоритма обычно выбирают минимальное изменение среднеквадратической ошибки. Так же возможно останавливать работу алгоритма, если на шаге 2 не было объектов, переместившихся из кластера в кластер.

К недостаткам данного алгоритма можно отнести необходимость задавать количество кластеров для разбиения.

\item Нечеткие алгоритмы

Наиболее популярным алгоритмом нечеткой кластеризации является алгоритм c-средних (c-means). Он представляет собой модификацию метода k-средних. Шаги работы алгоритма:

    Выбрать начальное нечеткое разбиение n объектов на k кластеров путем выбора матрицы принадлежности U размера n x k.
    Используя матрицу U, найти значение критерия нечеткой ошибки:
    Перегруппировать объекты с целью уменьшения этого значения критерия нечеткой ошибки.
    Возвращаться в п. 2 до тех пор, пока изменения матрицы U не станут незначительными.

Этот алгоритм может не подойти, если заранее неизвестно число кластеров, либо необходимо однозначно отнести каждый объект к одному кластеру.

\item Алгоритмы, основанные на теории графов

Суть таких алгоритмов заключается в том, что выборка объектов представляется в виде графа G=(V, E), вершинам которого соответствуют объекты, а ребра имеют вес, равный «расстоянию» между объектами. Достоинством графовых алгоритмов кластеризации являются наглядность, относительная простота реализации и возможность вносения различных усовершенствований, основанные на геометрических соображениях. Основными алгоритмам являются алгоритм выделения связных компонент, алгоритм построения минимального покрывающего (остовного) дерева и алгоритм послойной кластеризации.

\item Алгоритм выделения связных компонент

В алгоритме выделения связных компонент задается входной параметр R и в графе удаляются все ребра, для которых «расстояния» больше R. Соединенными остаются только наиболее близкие пары объектов. Смысл алгоритма заключается в том, чтобы подобрать такое значение R, лежащее в диапазон всех «расстояний», при котором граф «развалится» на несколько связных компонент. Полученные компоненты и есть кластеры.

Для подбора параметра R обычно строится гистограмма распределений попарных расстояний. В задачах с хорошо выраженной кластерной структурой данных на гистограмме будет два пика – один соответствует внутрикластерным расстояниям, второй – межкластерным расстояния. Параметр R подбирается из зоны минимума между этими пиками. При этом управлять количеством кластеров при помощи порога расстояния довольно затруднительно.

\item Алгоритм минимального покрывающего дерева

Алгоритм минимального покрывающего дерева сначала строит на графе минимальное покрывающее дерево, а затем последовательно удаляет ребра с наибольшим весом. 

Путём удаления связи, помеченной CD, с длиной равной 6 единицам (ребро с максимальным расстоянием), получаем два кластера: {A, B, C} и {D, E, F, G, H, I}. Второй кластер в дальнейшем может быть разделён ещё на два кластера путём удаления ребра EF, которое имеет длину, равную 4,5 единицам.

\item Послойная кластеризация

Алгоритм послойной кластеризации основан на выделении связных компонент графа на некотором уровне расстояний между объектами (вершинами). Уровень расстояния задается порогом расстояния c. 
Посредством изменения порогов расстояния {с0, …, сm}, где 0 = с0 < с1 < …< сm = 1, возможно контролировать глубину иерархии получаемых кластеров. Таким образом, алгоритм послойной кластеризации способен создавать как плоское разбиение данных, так и иерархическое. 

\end{enumerate}

\subsection{Сервисы рекомендаций}

Миллионы людей по всему миру пользуются музыкальными стриминговыми сервисами, то есть слушают песни, не скачивая их на устройства. Сегодня этот рынок обладает огромным потенциалом. За первую половину 2016 года количество аудиостримов в США удвоилось, по сравнению с 2015 годом.

Более того, к концу 2015 года количество подписчиков музыкальных стриминговых сервисов составило 68 миллионов по всему земному шару, и эта цифра продолжает расти. Сегодня на этом рынке работает множество популярных компаний начиная с зарубежных Spotify, Pandora, 8tracks и заканчивая российскими Яндекс.Музыка и Zvooq (рисунок 1.2).

\begin{figure}[ht]
\centering
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{Zvooq.png}  
  \caption{Интерфейс сервиса Zvooq}
  \label{fig:domain:zvooq}
\end{figure}

Пользователи используют стриминговые сервисы, потому что это удобно – не нужны физические носителями большого обьема, не нужно скачивать музыку к себе на устройство – все композиции находятся буквально в шаговой доступности. Но одна из главных причин популярности стриминга – это музыкальные рекомендации.

Многим людям хочется чего-то нового, потому такие сервисы, как, например, Tidal и Apple Music, предлагают подборки из песен, подходящих под наши музыкальные вкусы.

Чтобы составить плейлист, компаниями используется колоссальное количество данных, обрабатываемых машинными алгоритмами. 

Выделяют четыре подхода к анализу музыки для составления рекомендаций: использование данных о популярности композиции (количество прослушиваний и покупок песни) и мнений критиков, а также анализ текста и акустический анализ. Первые два типа анализа имеют один существенный недостаток – они не способствуют продвижению музыки малоизвестных исполнителей.

\subsubsection{Акустический и текстовый анализ}

Можно сказать, что история компании Echo Nest началась в тот момент, когда Уитман, будучи еще студентом, создал программу, анализирующую музыкальные блоги с применением технологий обработки естественных языков (natural language processing). Сегодня его алгоритм развился и постоянно изучает веб, просматривая около 10 миллионов страниц, имеющих отношение к музыке.

Любая фраза, которая появляется в интернете и имеет отношение к музыке, проходит через системы Echo Nest, выискивающие дескрипторы, ключевые слова и связанные с ними термины. При этом каждый термин имеет собственный вес, который говорит о его важности (по сути, он представляет собой вероятность того, что кто-то опишет песню этим словом). Списки рекомендаций формируются путем сопоставления выявленных дескрипторов с дескрипторами любимых песен пользователей.

Что же касается второго способа формирования рекомендаций – акустического анализа, то он не применяется сервисом в чистом виде. Пока еще нельзя говорить о качественном распознавании, например, музыкальных инструментов. Однако несмотря на это анализ сигнала играет очень важную роль в работе рекомендательных алгоритмов. Например, люди хотят, чтобы плейлисты были «гладкими»: после тихой и спокойной песни не может идти громкая, а в плейлистах, составленных для пробежек, темп должен постепенно возрастать.

Анализ песни начинается с того, что звук разбивается на небольшие кусочки, размером от 200 мс до 4 с, в зависимости от того, как быстро изменяется «рисунок» песни. Затем для каждого сегмента определяется громкость, тембр, также выявляются используемые музыкальные инструменты; устанавливается к какой части композиции (припев, куплет и т. д.) относится этот сегмент.

Далее, полученная информация объединяется и анализируется с помощью инструментов машинного обучения. Это дает нам возможность понять песню на «высоком уровне. После этого композиция получает особые метки (энергичность, живость и другие), которые выполняют описательную функцию.

Именно благодаря разработке таких мощных технологий компании Echo Nest удалось стать мировым лидером в алгоритмах анализа музыки. По этой причине в 2014 году её и купили титаны музыкального стриминга Spotify. Spotify – это мировой лидер стриминга музыки с 30 миллионами платных подписчиков. При этом компания получает тысячи восторженных отзывов о своих рекомендательных сервисах.

Своим успехом компания обязана коллаборативной фильтрации. Этот подход позволяет предсказывать предпочтения пользователя на основе его истории потребления контента – лайков, количества прослушиваний и др. – путем сравнения с данными других пользователей. Таким образом, алгоритм выявляет песни, идеально подходящие клиентам без вмешательства человека.

\subsubsection{Будущее рекомендательных сервисов}

Однако есть технологии, способные вывести рекомендательные сервисы на совершенно иной уровень. Сандер Дилеман (Sander Dieleman), исследователь в Google DeepMind, был соавтором статьи, в которой утверждалось, что нейронные сети и глубинное обучение могут справляться с рекомендациями аудио куда эффективнее коллаборативной фильтрации.

Дилеман начал исследовать возможности сверточных нейронных сетей (convolutional neural network) с 7–8 слоями. В частности, он использовал алгоритм t-SNE, который позволяет визуализировать многомерные данные. Сети, которые тренировал Дилеман, научились определять музыкальные инструменты, аккорды, и даже гармонии и прогрессии. Первый слой сети выделил 256 различных фильтров, например, «пение вибрато» и «бас-барабан». Более того, сеть самостоятельно нашла и объединила в плейлисты китайские поп-песни.

Решение Дилемана хорошо себя показало, и в случае успешного тестирования на реальной системе будет использоваться совместно с данными других алгоритмов. Однако стриминговые сервисы не останавливаются только на анализе песен и персональных музыкальных предпочтений клиентов.

Примерно год назад компания Spotify объявила о намерении собирать данные о местоположении, контактах и голосе пользователей. Спустя шесть месяцев, стало известно еще об одном нововведении: Spotify объединила усилия с компанией Runkeeper, с целью использовать физические данные клиентов для подборки треков, идеально подходящих к их темпу бега. Еще несколько лет назад такое показалось бы фантастикой.
